#!/bin/bash

docker build . -t hwlnginx

docker run -p 8081:80 -e NOODLE_IMAGE=spaghetti.jpg -e BG_COLOR=red --rm -it hwlnginx
