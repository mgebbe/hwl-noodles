#!/bin/bash

sed -i -r "s/NOODLE_IMAGE_PLACEHOLDER/${NOODLE_IMAGE}/g" /usr/share/nginx/html/index.html
sed -i -r "s/BG_COLOR_PLACEHOLDER/${BG_COLOR}/g" /usr/share/nginx/html/index.html

nginx -g 'daemon off;'
