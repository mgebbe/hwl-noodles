FROM nginx:latest
ADD html /usr/share/nginx/html
ADD hwl-docker-entrypoint.sh /
RUN chmod u+x hwl-docker-entrypoint.sh
ENV NOODLE_IMAGE "hellmann.svg"
ENV BG_COLOR "blue"
ENTRYPOINT ["/hwl-docker-entrypoint.sh"]
